To sign an url with HMAC SHA-1 from the command line you can simply do:

echo -n "100x200/smart/picture.png" | openssl dgst -sha1 -hmac "MY_SECURE_KEY" -binary | base64

making sure to use the result AFTER replacing + with - and / with _